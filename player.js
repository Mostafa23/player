class Player{
    constructor(musicList){
        this.container = document.querySelector('.player');
        this.audio = this.container.querySelector('audio');
        this.playBtn = this.container.querySelector('[data-action = "play"]');
        this.progressBar = this.container.querySelector('.progress .played');
        this.progress = this.container.querySelector('.progress');
        this.playListObj =  this.container.querySelector('.music');
        this.loopBtn = this.container.querySelector('[data-action="loop"]');
        this.muteBtn = this.container.querySelector('[data-action="mute"]');
        this.musicList = musicList;
        this.currentMusicIndex = 0;
        this._addPlayList();
        this._assingSrc(0);

        this._seekBarMoveWidthMouseMove = this._progressBarMoveWithClick.bind(this); // bug-fix for moving seekbar with mousemove

        this.container.addEventListener('click',(event) =>{
            let target = event.target;
            const action = target.dataset.action;
            if(action && `_${action}` in this){
                this[`_${action}`]();
            }
        })

        this.audio.addEventListener('play', this._onplay.bind(this));
        this.audio.addEventListener('pause', this._onpause.bind(this));
        this.audio.addEventListener('timeupdate',this._progressBar.bind(this));
        this.audio.addEventListener('ended',this._next.bind(this));
        this.progress.addEventListener('click', this._progressBarMoveWithClick.bind(this));
        this.progress.addEventListener('mousedown' , (event) =>{
            if(event.which == 1){
                this.progress.addEventListener('mousemove',this._seekBarMoveWidthMouseMove);
            }
            
        });
        this.progress.addEventListener('mouseup' , (event) =>{
            if(event.which == 1){
                this.progress.removeEventListener('mousemove',this._seekBarMoveWidthMouseMove);
            }
        });
        document.addEventListener('keydown',this._forwardSeekBackward.bind(this));
        document.addEventListener("syncIconPlayList",this._syncPlaylistIcon.bind(this));
        
    }

    _play(){
        this.audio.play();
    }
    _pause(){
        this.audio.pause();
    }

    _onplay(){
        this.playBtn.dataset.action = "pause";
        this.playBtn.textContent = "pause";
    }

    _onpause(){
        this.playBtn.dataset.action = "play";
        this.playBtn.textContent = "play_arrow";
    }

    _progressBar(){
        this.progressBar.style.width = `${(this.audio.currentTime / this.audio.duration) * 100}%`;
    }

    _progressBarMoveWithClick(event){
        let target = event.target;
        let xPositionSeekBar = (event.clientX - target.getBoundingClientRect().left) / target.offsetWidth;
        this.audio.currentTime = xPositionSeekBar * this.audio.duration;
        this.progressBar.style.width = `${(this.audio.currentTime / this.audio.duration) * 100}%`;
    }

    _previous(){
        let lastIndex = this.musicList.length - 1;
        this.currentMusicIndex = (this.currentMusicIndex - 1) < 0 ? lastIndex : this.currentMusicIndex - 1;
        this._assingSrc(this.currentMusicIndex);
        this._play();
    }

    _next(){
        let lastIndex = this.musicList.length - 1;
        this.currentMusicIndex = (this.currentMusicIndex + 1) > lastIndex ? 0 : this.currentMusicIndex + 1;
        this._assingSrc(this.currentMusicIndex);
        this._play();
    }

    _assingSrc(index){
        this.audio.src = this.musicList[index].src;
        document.dispatchEvent(new Event("syncIconPlayList"));
        this._changeMainTitleMusic();
    }

    _changeMainTitleMusic(){
        title.innerHTML = this.musicList[this.currentMusicIndex].title;
        singer.innerHTML = this.musicList[this.currentMusicIndex].singer;
        let coverObj = document.querySelector(".cover");
        let imageList = document.querySelectorAll(".img.first");
        coverObj.style.backgroundImage = 'url("./hayedeh.jpg")';
        imageList.forEach(elem => {
            elem.style.backgroundImage = 'url("./hayedeh.jpg")';
        })
    }

    _loop(){
        if(this.audio.loop){
            this.audio.loop = false;
            this.loopBtn.classList.remove('looping');
        }else{
            this.audio.loop = true;
            this.loopBtn.classList.add('looping');
        }
    }

    _mute(){
        if(this.audio.muted){
            this.audio.muted = false;
            this.muteBtn.innerHTML = "volume_up"
        }else{
            this.audio.muted = true;
            this.muteBtn.innerHTML = "volume_mute";
        }
    }

    _forwardSeekBackward(event){
        if(event.which == 39){
            this.audio.currentTime = this.audio.currentTime + 5 > this.audio.duration ? this.audio.currentTime :  this.audio.currentTime + 5;
        }
        if(event.which == 37){
            this.audio.currentTime = this.audio.currentTime - 5 < 0 ? this.audio.currentTime :  this.audio.currentTime - 5;
        }
    }

    _syncPlaylistIcon(){
        const icon = document.querySelector(".state.playing");
        icon.classList.remove('playing');
        icon.firstElementChild.textContent = "play_arrow";

        const currentMusic = document.querySelectorAll('[data-index]');
        currentMusic[this.currentMusicIndex].lastElementChild.classList.add('playing');
        document.querySelector(".state.playing").firstElementChild.textContent = "equalizer";
    }

    _addPlayList(){
        let templatePlayList = document.querySelector('.templatePlayList');
        for(let [index,elementMusic] of this.musicList.entries()){
            let cloneTemplateContent = templatePlayList.content.cloneNode(true);
            cloneTemplateContent.firstElementChild.innerHTML = cloneTemplateContent.firstElementChild.innerHTML.replace(/{{\s*(.*?)\s*}}/g,(matchItem)=>{
                if(matchItem === "{{title}}"){
                    return elementMusic.title;
                }
                if(matchItem === "{{singer}}"){
                    return elementMusic.singer;
                }

                if(matchItem === "{{icon}}"){
                    if(this.currentMusicIndex === index){
                        return "equalizer";
                    }else{
                        return "play_arrow";
                    }
                }

                if(matchItem === "{{playing}}"){
                    if(this.currentMusicIndex === index){
                        return "playing";
                    }else{
                        return "";
                    }
                }

            });
            cloneTemplateContent.querySelector('[data-action="changesource-playlist"]').addEventListener('click',() =>{
                this.currentMusicIndex = index;
                this._assingSrc(index);
                this._play();
            })
            cloneTemplateContent.firstElementChild.setAttribute("data-index" , index);
            this.playListObj.append(cloneTemplateContent);
        }
    }

}

